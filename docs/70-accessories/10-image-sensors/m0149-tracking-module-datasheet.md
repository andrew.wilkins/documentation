---
layout: default
title: M0149 Tracking Module Datasheet
parent: Image Sensors
nav_order: 149
has_children: false
permalink: /M0149/
---

# VOXL 2 Tracking Sensor Datasheet
{: .no_toc }

## MSU-M0149-1-01

![M0149.JPG](/images/other-products/image-sensors/M0149_angle_1920x1920.jpg)

{:toc}

### Specification

| Specification | Value                                                                                |
|----------------|--------------------------------------------------------------------------------------|
| Sensor         | OnSemi AR0144 [Datasheet]() |
| Shutter        | Global                                                                               |
| Resolution     | 1280x800                                                                             |
| Framerate      |                                      |
| Data formats   |                                                                       |
| Lens Size      |                                                                                |
| Focusing Range |  |
| Focal Length   |                                                                                 |
| F Number       |                                                                                    |
| Fov(DxHxV)     |                                                                     |
| TV Distortion  |                                                                                |


### Technical Drawings

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0149-AR0144.STEP)

#### 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0149_2D_02-24-24.pdf)

### Pin Out

![M0149_pin1.JPG](/images/other-products/image-sensors/M0149_pin1.jpg)

## Image Samples for Sensor

### Indoor

### Outdoor

