---
layout: default
title: Sentinel Vehicle Components
parent: Sentinel User Guide
nav_order: 5
has_children: false
permalink: /sentinel-user-guide-components/
---

# Sentinel Vehicle Components
{: .no_toc }

1. TOC
{:toc}

## Overview

The following images give an overview of the vehicle components.

### Front
[View in fullsize](/images/sentinel/sentinel-labeled-1.png){:target="_blank"}
![Sentinel-Labeled-1](/images/sentinel/sentinel-labeled-1.png)

### Side
[View in fullsize](/images/sentinel/sentinel-labeled-2.png){:target="_blank"}
![Sentinel-Labeled-2](/images/sentinel/sentinel-labeled-2.png)

### Top
[View in fullsize](/images/sentinel/sentinel-labeled-3.png){:target="_blank"}
![Sentinel-Labeled-3](/images/sentinel/sentinel-labeled-3.png)

### Bottom
[View in fullsize](/images/sentinel/sentinel-labeled-4.png){:target="_blank"}
![Sentinel-Labeled-4](/images/sentinel/sentinel-labeled-4.png)

[Next Step: Power and Battery](/sentinel-user-guide-power/){: .btn .btn-green }