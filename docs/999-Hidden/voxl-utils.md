---
layout: default
title: VOXL Utilities
nav_exclude: true
search_exclude: true
permalink: /voxl-utils/
---

# VOXL Utilities
{: .no_toc }

## This page is deprecated
{: .no_toc}
Please refer to [Tools and Utilities](/sdk-utilities/) for on-board utilities.


## voxl-wifi

This utility is used to setup the Wi-Fi mode.  The VOXL can be setup in `station` mode, in which it connects to an Access Point, or it can be setup in `softap` mode, where it itself acts as an Access Point, allowing clients to connect to it.

For details on how to use voxl-wifi, go to the [WiFi Setup page](/wifi-setup/).



## voxl-env

The `voxl-env` utility can be used to inspect ROS and VOXL-specific bash environment variables.

```bash
/# voxl-env
ROS_MASTER_URI=http://localhost:11311/
ROS_IP=192.168.1.56
CAM_CONFIG_ID=1
HIRES_CAM_ID=-1
TRACKING_CAM_ID=0
STEREO_CAM_ID=1
TOF_CAM_ID=-1
```

## voxl-version

The `voxl-version` command displays the various version information.

```bash
me@mylaptop:~$ adb shell voxl-version
--------------------------------------------------------------------------------
system-image:    ModalAI 1.10.0 BUILDER: ekatzfey BUILD_TIME: 2019-11-11_23:05
kernel:          #1 SMP PREEMPT Mon Nov 11 22:28:13 UTC 2019 3.18.71-perf
factory-bundle:  0.0.2
sw-bundle:       0.0.2
--------------------------------------------------------------------------------
architecture:    aarch64
processor:       apq8096
os:              GNU/Linux
--------------------------------------------------------------------------------
voxl-utils:
Package: voxl-utils
Version: 0.4.3
Status: install user installed
Architecture: aarch64
Installed-Time: 14

--------------------------------------------------------------------------------
```




## voxl-configure-cameras

Configures camera IDs for [ROS](/voxl-nodes/), [VOXL Camera Manager](/voxl-cam-manager/),(ModalAI Vision Library)[/modalai-vision-lib/], and other utilities that use cameras.

See the [Camera Configuration page](/configure-cameras/) for more info.

```bash
/# voxl-configure-cameras -h
General Usage:
voxl-configure-cameras <configuration-id>

If no configuration-id is given as an argument, the user will be prompted.

show this help message:
voxl-configure-cameras media_scripts -h

available camera configurations are as follows:
1 Tracking + Stereo (default)
2 Tracking Only
3 Hires + Stereo + Tracking
4 Hires + Tracking
5 TOF + Tracking
6 Hires + TOF + Tracking
7 TOF + Stereo + Tracking
```
