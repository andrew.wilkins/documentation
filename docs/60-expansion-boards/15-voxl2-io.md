---
layout: default3
title: VOXL 2 IO
nav_order: 15
parent: Expansion Boards
has_children: true
permalink: /voxl2-io/
thumbnail: /voxl2-io/M0065-hero.png
buylink: https://www.modalai.com/products/voxl2-io
summary: VOXL 2 IO adds PWM output along with S.BUS radio and Spektrum radio connections to VOXL 2 or VOXL 2 mini.
---

# VOXL 2 IO
{: .no_toc }

**Important Update**

Starting in VOXL SDK 1.1.1, VOXL 2 IO is being updated:
- new bootloader based on same bootloader as VOXL ESC (requires in field update, or request replacement by sending in existing hardware)
- new firmware based on same firmware as VOXL ESC
- new `voxl-px4` driver `voxl2-io`
- for upgrade instructions, see the [firmware guide](/voxl2-io-firmware/)

VOXL 2 IO is meant to be paired with VOXL 2 and adds PWM output along with S.BUS radio and Spektrum radio connections.  It communicates over UART to VOXL 2 running PX4.


For documentation on the legacy PX4IO/VOXL SDK 1.1.0 and older usage, see [here](/voxl2-io-legacy-guide/)

<a href="https://www.modalai.com/products/voxl2-io" style="background:none;"><button type="button" class="btn2" style="margin-right:1em;">Buy Here</button></a>
<a href="https://forum.modalai.com/" style="background:none;"><button type="button" class="btn3" style="margin-left:0.5em;"> Support </button></a>

<img src="/images/voxl2-io/M0065-hero.png" width="240"/>
