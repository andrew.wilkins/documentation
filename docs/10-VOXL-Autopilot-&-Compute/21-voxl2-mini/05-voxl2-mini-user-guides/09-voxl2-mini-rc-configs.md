---
layout: default
title: VOXL 2 Mini RC Configs
parent: VOXL 2 Mini User Guides
nav_order: 9
permalink:  /voxl2-mini-rc-configs/
---

# VOXL 2 Mini Remote Controller Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

We are working to expand the capabilities of VOXL 2 every day. The built-in PX4 flight controller enables industry leading SWAP for an autonomous flight controller, but not every interface and flight controller are supported yet. This page provides an overview of available connectivity. If this connectivity is insufficient for your application, VOXL 2 is a world-class companion computer for autonomous navigation and AI when paired with an [external flight controller](/voxl2-external-flight-controller/).

## Remote Controller Options for Built-in Flight Controller

| Interface         | Protocol     | Includes Telemetry | Instructions                                                                                       | Adapter                                          | Example Hardware |
|-------------------|--------------|--------------------|----------------------------------------------------------------------------------------------------|--------------------------------------------------|------------------|
| ELRS              | CRSF/UART    | Yes                | CRSF raw                                                                                           |                                                  | Beta FPV         |
| TBS Crossfire     | UART         | Yes                | MAVLink and CRSF raw                                                                               |                                                  | Nano             |
| WiFi              | IP / Mavlink | No                 | [VOXL 2 Wifi Dongle User Guide](/voxl2-wifidongle-user-guide/) [Connect QGC over WiFi](/qgc-wifi/) |                                                  |                  |
| Microhard         | IP / Mavlink | No                 | [Microhard Add-on Manual](/microhard-add-on-manual/)                                               |                                                  |                  |
| Doodle Labs       | IP / Mavlink | No                 | [Doodle Labs User Guide](/doodle-labs-user-guide/)                                                 |                                                  |                  |
| Cellular          | IP / Mavlink | No                 | [4G](/lte-v2-modem-user-guide/) or [5G](/5G-Modem-user-guide) with [voxl-vpn](/voxl-vpn/)          |                                                  |                  |
| Spektrum (DSMX)   | UART         | No                 | Not yet supported                                                                                  |                                                  |                  |
| Graupner          |              | No                 | Not yet supported                                                                                  |                                                  |                  |
| FrSky             |              | No                 | Not yet supported                                                                                  |                                                  |                  |
| Futaba            |              | No                 | Not yet supported                                                                                  |                                                  |                  |

## CRSF

### Hardware

**PLEASE NOTE**: default CRSF baud rate used by `voxl-px4` is 420k.

- [CRSF baud](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/voxl2/target/voxl-px4.config#L88)
- [MAVLink baud](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/voxl2/target/voxl-px4.config#L91)

Tested hardware:
- TBS Crossfire Nano Rx (in both CRSF and MAVLink mode)
- Beta FPV 900 Nano

<img src="/images/voxl2-mini/m0104-user-guides-rc.jpg" alt="m0104-user-guides-rc" width="1280"/>

M0104 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

### Software

#### Setting PX4 to Use CRSF

See file at `/etc/modalai/voxl-px4.conf`.

This file is loaded from [boards/modalai/voxl2/target/voxl-px4](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/voxl2/target/voxl-px4).

The previous script calls this script with command line args to start CRSF, [boards/modalai/voxl2/target/voxl-px4-start](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/voxl2/target/voxl-px4-start).

#### Checking Status

The `px4-listener input_rc` or `px4-listener rc_channels` command can be used to trouble shoot, if neither have been published it's likely the receiver has yet to get RC data or the serial link isn't working.

#### PX4 Driver

[src/drivers/rc/crsf_rc](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/rc/crsf_rc)