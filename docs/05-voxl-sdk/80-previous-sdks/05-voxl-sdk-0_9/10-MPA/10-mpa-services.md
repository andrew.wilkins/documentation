---
layout: default
title: MPA Services 0.9
parent: Modal Pipe Architecture 0.9
search_exclude: true
nav_order: 10
has_children: true
has_toc: true
permalink: /mpa-services-0_9/
---

# MPA Services

A collection of MPA services that run in the background and perform tasks to provide raw and processed sensor data through MPA pipes in the filesystem.
