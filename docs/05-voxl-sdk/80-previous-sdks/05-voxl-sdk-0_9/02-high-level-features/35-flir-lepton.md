---
layout: default
title: FLIR Lepton 0.9
parent: High Level Features 0.9
search_exclude: true
nav_order: 50
permalink: /voxl-flir-server-0_9/
---

# FLIR Lepton
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

voxl-flir-server is an application to send video frames from the FLIR Lepton SPI camera to MPA pipes.

For Flir UVC enabled cameras with USB interfaces such as the Boson 640 and PureThermal Mini Pro with Lepton use [voxl-uvc-server](https://docs.modalai.com/voxl-uvc-server/) instead.


## Connecting

On VOXL1, the FLIR Lepton should be connected to SPI bus 9.
On VOXL2, the FLIR Lepton should be connected to SPI bus 14.

TODO make a wiring diagram.


## Set up voxl-flir-server

voxl-flir-server was introduced in VOXL SDK 0.9, and is available in the voxl package repos but is not installed by default.

To install on VOXL2:
```bash
apt update
apt install voxl-flir-server
```

To install on VOXL1
```bash
opkg update
opkg install voxl-flir-server
```

On both platforms, you can run the voxl-flir-server binary manually from the command line for debugging and testing, or enable the systemd background service to start it automatically on boot:

```bash
systemctl enable voxl-flir-server
systemctl start voxl-flir-server
```




## Source

Source code is [here](https://gitlab.com/voxl-public/modal-pipe-architecture/voxl-flir-server/-/tree/dev).
