---
title: SDK 1.0 Release Notes
layout: default
parent: VOXL SDK Release Notes
has_children: false
nav_order: 50
permalink: /sdk-1.0-release-notes/
---

# SDK 1.0 Release Notes

SDK 1.0 Currently only supports QRB5165 platforms VOXL2 and VOXL2-Mini. APQ8096 (VOXL1) support is in progress for a future patch release.

All SDK-1.0.x releases on QRB5165 use the same 1.6.2 system image.  See [here](/voxl2-system-image/) for system image release notes.



## New Features in SDK 1.0

SDK 1.0 is our biggest release yet with 8 months of work, hundreds of commits, and countless improvements. A few of the highlights are as follows:


### Streamlined Configuration

The primary focus has been to streamline MPA configuration process by including it in the SDK install script. A new package, [voxl-configurator](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-configurator) consolidates tools for managing the [SKU](/sku/), [calibration files](/check-calibration/), and [configuring MPA services](/voxl-configure-mpa/). All of these tools are called during the [SDK flashing script](/flash-system-image/#flashing-a-voxl-sdk-release). Furthermore, [ELRS](voxl-elrs) and [VOXL ESC](/voxl-escs/) firmware and parameters are automatically flashed during SDK installation to keep everything updated and in sync automatically.

### PX4 1.14

Since ModalAI's initial fork of PX4 1.12 and port to the VOXL SDSP, we have been working closely with the PX4 community to merge our multi-processor architecture support back into PX4 mainline. See the [voxl-px4](/voxl-px4/) page for more information.


### Hardware-Accelerated Video compression in voxl-camera-server

For hires color cameras, voxl-camera-server can now publish either H265 or H264 compressed video over libmodal-pipe. Furthermore, it can compress two video streams at different resolutions simultaneously. Out of the box on the Sentinel and Starling platforms, voxl-camera-server publishes both a 1024x768 H265 stream for RTSP video streaming and 4K video for recording to disk with the new voxl-record-video tool. Both at 30hz, accelerated in hardware! This enabled the next feature:

### Always-on RTSP video and Mavlink Camera Protocol Support

Out of the box, in hardware configurations with a color hires cameras, voxl-streamer and voxl-mavcam-manager are enabled by default so that as soon as you connect to QGroundControl you get a live video feed with no additional configuration steps. Furthermore, the video snapshot and video record functions in QGgroundControl will automatically appear and allow you to take full resolution JPEG snapshots and 4K video recordings that get saved to disk onboard VOXL in flight.

### Mavlink Routing Overhaul

When running on the VOXL SDSP, [voxl-px4](/voxl-px4/) now provides two simultaneous mavlink interfaces. One high datarate interface for local communication onboard voxl, and a second low datarate interface to communicate with a ground control station. Multiple bi-directional pipes expose these interfaces to allow onboard data such as VIO to be transmitted to PX4 and services like voxl-mavcam-manager to communicate with the grans control station. A new tool voxl-inspect-mavlink lets you inspect all of these pathways for real time debugging.


### ELRS Support

VOXL now supports ELRS Receivers and starting with the Starling development drone this is ModalAI's new recommended RC control interface. Baked into SDK 1.0 is a new [voxl-elrs](/voxl-elrs/) tool that allows flashing ESP-based ELRS receiver firmware and putting the receivers into bind mode from the VOXL command line.

### VOXL-ESC Flashing and Tools

[VOXL ESCs](/voxl-escs/) can still be flashed and tuned from a desktop PC like before, but these same tools now run onboard VOXL. This allows ESC firmware updates automatically during SDK flashing.






## V1.0.0

Released June 30, 2023

Package list and changes from SDK-0.9.5

| Package | Version | APQ8096 | QRB5165|
|:---|:---:|:---:|:---:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.3.1 (unchanged) | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.3 (unchanged) | ✅ |  |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng) | 1.6.38-1 (unchanged) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.3 (unchanged) | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.3 --> 0.2.1 | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.8.3-1 (unchanged) | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.6.0 (unchanged) | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.2.3 --> 0.3.2 | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (unchanged) | ✅ | ✅ |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal) | 0.2.1 --> 0.2.2 | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.3 (unchanged) | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.8.2 --> 2.8.4 | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 --> 0.2.0 |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.3.0 --> 1.4.2 | ✅ | ✅ |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct) | 0.1.5 (unchanged) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (unchanged) | ✅ | ✅ |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.1.0 (unchanged) |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.5.0 --> 0.6.0 |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.5 --> 0.2.2 |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (unchanged) |  | ✅ |
| [voxl-bind-spektrum](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind-spektrum) | 0.1.0 (new) |  | ✅ |
| [voxl-boost](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-boost) | 1.65.0 (new) | ✅ |  |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.2.3 --> 0.4.0 | ✅ | ✅ |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server) | 1.3.5 --> 1.6.2 | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-9 (unchanged) | ✅ | ✅ |
| [voxl-configurator](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-configurator) | 0.2.7 (new) | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.3.0 --> 0.4.6 | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.2.4 --> 1.2.5 | ✅ | ✅ |
| [voxl-eigen3](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-eigen3) | 3.4.0 (unchanged) | ✅ | ✅ |
| [voxl-elrs](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-elrs) | 0.0.7 (new) |  | ✅ |
| [voxl-esc](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-esc) | 1.2.0 (new) |  | ✅ |
| [voxl-feature-tracker](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-feature-tracker) | 0.0.7 --> 0.2.3 |  | ✅ |
| [voxl-flow-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flow-server) | 0.3.3 (new) |  | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (unchanged) | ✅ | ✅ |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (unchanged) | ✅ | ✅ |
| [voxl-lepton-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-lepton-server) | 1.1.2 (new) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.7 (unchanged) | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.3.4 (unchanged) | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.1.5 --> 0.1.7 | ✅ | ✅ |
| [voxl-mavcam-manager](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavcam-manager) | 0.5.1 (new) | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.1.0 --> 0.1.1 | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.3.0 --> 1.2.0 |  | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.16.1 --> 1.0.5 | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.7.0-1 (unchanged) | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.7.6 --> 1.0.4 | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.6 (unchanged) | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (unchanged) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (unchanged) | ✅ | ✅ |
| [voxl-open-vins](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-open-vins) | 0.0.2 --> 0.3.0 | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.5.0 --> 0.5.8 | ✅ | ✅ |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) | 1.12.31 --> 1.14.0-2.0.34 |  | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.2 (unchanged) |  | ✅ |
| [voxl-px4-params](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-px4-params) | 0.1.8 (new) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.8.2 --> 0.9.3 | ✅ | ✅ |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) | 0.0.5 --> 0.0.8 |  | ✅ |
| [voxl-slpi-uart-bridge](https://gitlab.com/voxl-public/voxl-sdk/core-libs/voxl-slpi-uart-bridge) | 1.0.1 (new) |  | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer) | 0.4.1 --> 0.7.1 | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.3.1 (unchanged) | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 1.2.2 --> 1.3.1 | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.3 --> 0.1.6 | ✅ | ✅ |
| [voxl-vision-hub](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-hub) | 1.6.6 (new) | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.1.3 (unchanged) | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.6 (unchanged) | ✅ |  |


