---
layout: default
title: Inspect Tools
parent: VOXL SDK
search_exclude: true
nav_order: 50
has_children: true
has_toc: true
permalink: /inspect-tools/
---

# Inspect Tools

Most pipes in the VOXL SDK have some sort of `voxl-inspect-xxx` tool to allow easy debug and visualization of data.

To find a complete list of the inspection utilities, type `voxl-inspect-` in a VOXL shell, and press tab.

Many of these tools are part of the [voxl-mpa-tools project on GitLab](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools/).
