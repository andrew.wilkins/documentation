---
layout: default
title: VOXL CAM Flight Core Setup
parent: VOXL CAM User Guide
nav_order: 5
permalink: /voxl-cam-user-guide-flight/
---

# **VOXL-CAM v1 Flight Core Setup**
{: .no_toc }
 

![Voxl-cam-flightcore](/images/voxl-cam/voxl-cam-flightcore.png)

<p style="color:darkgreen"> <b> This guide is for VOXL-CAM with Flight Core </b> <br>
(SKU C3-FC1-D1, C7-FC1-D1, 	C3-FC1-M8, C7-FC1-M8, C3-FC1-M5, C7-FC1-M5) </p>

---

VOXL-CAM contains a Flight Core. Check out the flight core docs [Here](https://docs.modalai.com/flight-core-getting-started/) for more details.

---

<details open markdown="block">
  <summary>
    Table of Contents
  </summary>
  {: .text-echo }
1. [Datasheet, v1](/voxl-cam-v1-datasheet/)
2. Second Step
{:toc}
</details>

## Connectors 
See [Here](/flight-core-datasheets-connectors/) and [Here](/flight-core-connections/)

## Integrating with ESC's 
See [here](/flight-core-pwm-esc-calibration/) and [here](/modal-esc-v2-manual/)

## RC Setup
See [Here](/flight-core-radios/)

## Preflight Checks
See [Here](/voxl-m500-getting-started/#preflight-checks)

## First Flights
See [Here](/fc-first-flight/)

[Next Step: Modem](/voxl-cam-user-guide-modem/){: .btn .btn-green }