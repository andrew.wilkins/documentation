---
layout: default
title: 13. Connect to QGC
parent: VOXL Developer Bootcamp
nav_order: 13
permalink: /qgc-wifi/
---

# How to Connect VOXL to QGroundControl over WiFi
{: .no_toc }


Mavlink routing on VOXL is handled by [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) which routes mavlink messages between the autopilot, MPA services, and ground control stations.

---

## Video

{% include youtubePlayer.html id="QcAgAjAQDvQ" %}


## Prerequisites

### Validate required services are running

Run `voxl-inspect-services` to confirm that both voxl-px4 and voxl-mavlink-server are running.

![services](/images/voxl-developer-bootcamp/voxl-inspect-services.png)


### VOXL and Host PC should be able to ping each other

See the [VOXL Developer Bootcamp WiFi Guide](/wifi-guide/).



## Choosing which device should initiate the connection

There are two ways to initiate a connection between VOXL and a ground control station. Either VOXL can be configured to remember the IP address of the ground control station or visa versa. Choose which method suites your preference and specific use case.



## Initiating Connection to VOXL from QGroundControl

If you know the IP address of VOXL, you can point your GCS to VOXL's IP and initiate a connection with no configuration of VOXL whatsoever.

First you can determine the IP address of VOXL by using [ADB shell](/setting-up-adb/) over a USB cable and using the `voxl-my-ip` tool or standard Linux tools like `ifconfig`.

```bash
voxl2:/$ voxl-my-ip
wlan0: 192.168.1.225
```

In QGroundControl add VOXL's IP Address as a UDP connection in the Application Settings > Comm Links menu. Unless you expect to have multiple drones turned on on the same network at the same time, I suggest enabling the 'automatically connect on start' feature here.

Note, after typing in the IP address, you MUST click the add server button in QGroundControl.

![qgc_add_ip.png](/images/voxl-sdk/qgc/qgc_add_ip.png)

Once you've saved the connection information. Click the connect button.

![qgc_connect_to_ip.png](/images/voxl-sdk/qgc/qgc_connect_to_ip.png)

Then you can navigate back to the QGroundControl main window and you should be connected!

![qgc_connect_to_ip.png](/images/voxl-sdk/qgc/qgc_connect_to_ip.png)


## Initializing Connection to QGroundControl from VOXL

If you have a network architecture where the VOXL changes IP frequently but the GCS is at a static IP, then you can alternatively configure VOXL to initiate the connection to the GCS. To do this, add your GCS IP address to VOXL's `/etc/modalai/voxl-mavlink-server.conf` configuration file.

``` bash
voxl2:/$ nano /etc/modalai/voxl-mavlink-server.conf
	"primary_static_gcs_ip":	"192.168.8.10",
	"secondary_static_gcs_ip":	"192.168.8.11",
```

Then restart the mavlink server to load in the new config file.

```bash
voxl2:/$ systemctl restart voxl-mavlink-server
```

Note there are two fields allowing you to enter the IPs of up to two ground control stations. This is useful if you have both a desktop in the office and a laptop for field use. Note that the two default IPs in this config file are `192.168.8.10` `192.168.8.11`. These are the first two IPs that will be assigned by VOXL's DHCP server when devices connect to VOXL's SoftAP wireless network. This is what allows a GCS to auto-connect to VOXL when using SoftAP WiFi mode.

We suggest Leaving the primary ip as `192.168.8.10` so as to not disable the SoftAP auto connect functionality. Usually you will want to set your main GCS IP address to the secondary slot. There is no practical different between the primary and secondary slots, they both function the same and are only differentiated by name.



## Related Pages

See the [VOXL SDK Mavlink Routing](/mavlink/) page for more details.

Also see the [VOXL SDK 4G/5G QGround Control Connection Guide](/sentinel-user-guide-connect-gcs/) once you are done with the bootcamp to try flying with one of our [Modems](/modems/).


<br>
[Next: First Flight](/first-flight/){: .btn .btn-green }
